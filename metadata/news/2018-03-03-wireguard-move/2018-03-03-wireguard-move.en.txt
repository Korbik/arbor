Title: net/wireguard::arbor moved to net-misc/wireguard::net
Author: Wulf C. Krueger <philantrop@exherbo.org>
Content-Type: text/plain
Posted: 2018-03-03
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net/wireguard

WireGuard, formerly known as net/wireguard::arbor, has been moved to
net-misc/wireguard::net. OpenVPN, OpenSWAN and many others already live there
as will WireGuard happily ever after.

Please install net-misc/wireguard and *afterwards* uninstall net/wireguard.

1. Take note of any packages depending on net/wireguard:
cave resolve \!net/wireguard

2. Install net-misc/wireguard:
cave resolve net-misc/wireguard -zx

3. Re-install the packages from step 1.

4. Uninstall net/wireguard
cave resolve \!net/wireguard -x

Do it in *this* order or you'll *break* WireGuard.

