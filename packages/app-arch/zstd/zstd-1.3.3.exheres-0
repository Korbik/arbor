# Copyright 2017 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=facebook tag=v${PV} ]
require cmake [ api=2 ]

SUMMARY="Zstandard - Fast real-time compression algorithm"
DESCRIPTION="
Zstandard is a real-time compression algorithm, providing high compression ratios. It offers a very
wide range of compression / speed trade-off, while being backed by a very fast decoder. It also
offers a special mode for small data, called dictionary compression, and can create dictionaries
from any sample set.
"
HOMEPAGE="http://facebook.github.io/zstd/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SOURCE="${WORKBASE}"/${PNV}/build/cmake

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DZSTD_LEGACY_SUPPORT:BOOL=OFF
    -DZSTD_BUILD_CONTRIB:BOOL=ON
    -DZSTD_BUILD_PROGRAMS:BOOL=ON
    -DZSTD_BUILD_TESTS:BOOL=ON

    -DZSTD_ZLIB_SUPPORT:BOOL=OFF
    -DZSTD_LZMA_SUPPORT:BOOL=OFF
)

src_prepare() {
    cmake_src_prepare

    # Don't install into /usr/<host>/share
    edo sed -e '/DESTINATION "share/s@share@/usr/&@' \
            -i {lib,programs,contrib}/CMakeLists.txt
}

